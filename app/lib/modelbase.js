var Class = Base;

ModelBase = Class.extend({  
  _id: null,
  findOne: function(params){
    var result = this.self().collection.findOne(params);
    return this.self().new(result);
  },
  find: function(params){
    return this.self().collection.find(params);
  },
  hasValid: function(){
    return true;
  },
  persisted: function(){
    return (this._id !== null);
  },
  save: function(){
    if(!this.hasValid())
      return false;

    if(this.persisted()){
      if(this.beforeUpdate)this.beforeUpdate();
      this.collection.update({_id: this._id}, {$set: this.attributes});
    }else{
      if(this.beforeInsert)this.beforeInsert();
      this._id = this.collection.insert(this.attributes);
    }
    return true;
  },
  remove: function(){
    if(this.persisted())
      this.collection.remove({_id: this._id});
  }
});