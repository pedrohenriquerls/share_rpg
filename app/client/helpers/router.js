Router.configure({
  layoutTemplate: 'main',
  disableProgressSpinner : true
});

Router.map( function () {
	this.route('index', {
		path: '/',
    disableProgress: true,
    data:{
      hasHome: true
    }
	});
});