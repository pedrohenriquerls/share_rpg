(function () {
    "use strict";

    var createRoute = function (route, handler) {
        var connectHandlers;
        if (typeof __meteor_bootstrap__.app !== 'undefined') {
            connectHandlers = __meteor_bootstrap__.app;
        } else {
            connectHandlers = WebApp.connectHandlers;
        }
        connectHandlers.stack.splice(0, 0, {
            route: '/' + route,
            handle: function (req, res) {
                res.writeHead(200, {'Content-Type': 'text/plain'});
                handler(req, res);
                res.end(route + ' complete');
            }.future()
        });
    };

    Meteor.startup(function () {

    });
})();