(function () {

  "use strict";

  describe("Model Base", function () {
    var emptyFunction = function(){ return "nothing!"; };
    var collectionMethods = {
        insert: function(params){
          return 0;
        },
        find: function () {
            return {
                fetch: emptyFunction,
                observeChanges: emptyFunction
            };
        },
        findOne: function(params){
          return {name: 'Desafio do leite', description: 'Beber três litros de leite em 30 min.'};
        },
        update: emptyFunction,
        remove: emptyFunction,
        allow: emptyFunction
      };

    var Challenge;
    var challenge;

    beforeEach(function() {
      Meteor.SmartCollection.prototype = collectionMethods;

      var collection = new Meteor.SmartCollection("Challenges");
      Challenge = ModelBase.extend(collection, {
        initialize: function initialize(properties){
          this.name = properties.name;
          this.description = properties.description;
        },
        collection: collection
      });

      

      challenge = Challenge.new({name: 'Desafio do leite', description: 'Beber três litros de leite em 30 min.'});
    });

    it("Testa instancia de um model para uma collection", function () {
      expect(Challenge.isPrototypeOf(Challenge.new({}))).toBe(true);

      expect(Challenge.isPrototypeOf({})).toBe(false);
    });

    it("Testa chamada dos metodos do SmartCollection", function(){
      expect(Challenge.collection).not.toBeUndefined();

      var findOneResult = Challenge.findOne({});
      expect(true).toBe(Challenge.isPrototypeOf(findOneResult));
      expect('Desafio do leite').toMatch(findOneResult.name);
      expect('Beber três litros de leite em 30 min.').toMatch(findOneResult.description);
      //expect(Challenge.find({})).toMatch("Find something");

      expect(null).toBe(challenge._id);
      expect(false).toBe(challenge.persisted());
      expect(true).toBe(challenge.save());
      
      expect(true).toBe(challenge.persisted());
      expect(0).toMatch(challenge._id);
      //expect(challenge.update()).toMatch("Insert something");
    });

    it("Testa atributos de uma instancia", function(){
      expect(challenge.name).toMatch('Desafio do leite');
      expect(challenge.description).toMatch('Beber três litros de leite em 30 min.');

      expect(challenge.time).toBeUndefined();
    });
  });
})();