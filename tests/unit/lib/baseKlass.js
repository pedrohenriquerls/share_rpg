(function () {

  "use strict";

  describe("Classe para javascript", function () {

    it("Testa Class", function () {
      expect(Base.isPrototypeOf(Base.new())).toBe(true);
      expect(Base.isPrototypeOf(Base.extend())).toBe(true);
      expect(Base.isPrototypeOf(Base.extend().new())).toBe(true);

      expect(Base.extend().isPrototypeOf(Base.extend())).toBe(false);
      expect(Base.extend().isPrototypeOf(Base.extend())).toBe(false);
      expect(Base.extend().isPrototypeOf(Base.new())).toBe(false);
      
      expect(Base.new().isPrototypeOf(Base.extend())).toBe(false);
      expect(Base.new().isPrototypeOf(Base.new())).toBe(false);
    });

    it("Testa herança", function(){
      var Parent = Base.extend({
        name: "Mr. White",
        sayMyName: function () {
          return this.name + "!";
        }
      });

      expect(Parent.name).toMatch("Mr. White");
      expect(Parent.sayMyName()).toMatch("Mr. White!");

      expect(Parent.new().name).toEqual(Parent.name);
      expect(Parent.new().sayMyName()).toEqual(Parent.sayMyName());

      expect(Parent.extend({}).name).toEqual(Parent.name);
      expect(Parent.extend({}).sayMyName()).toEqual(Parent.sayMyName());

      var Child = Parent.extend({ name: "Heisenberg" });

      expect(Child.new().isPrototypeOf(Parent)).toBe(false);

      expect(Child.new().sayMyName).not.toBeUndefined();

      expect(Parent.name).not.toEqual(Child.name);
      expect(Child.new().name).toEqual(Child.name);
      expect(Child.extend().name).toEqual(Child.name);

      expect(Child.new().name).not.toMatch("Mr. White");
      expect(Child.new().sayMyName()).not.toMatch("Mr. White!");
      
      expect(Child.sayMyName).toBe(Parent.sayMyName);
      expect(Child.extend().sayMyName).toBe(Parent.sayMyName);
      expect(Child.new().sayMyName).toBe(Parent.sayMyName);

      expect(Child.new().sayMyName()).toMatch("Heisenberg!");
    });

    it("Testa chamada do construtor da classe pai", function(){
      var Father = Base.extend({
        initialize: function Foo(options) {
          this.name = options.name;
        },
        sayMyName: function () {
          return this.name + "!";
        }
      });

      var DrugDealer = Father.extend({
        initialize: function DrugDealer(options) {
          Father.initialize.call(this, options);
          this.obs = 'Vendedor de metaanfetamina azul!';
        }
      });

      var drugDealer = DrugDealer.new({ name: 'Heisenberg' });

      expect(Father.isPrototypeOf(drugDealer)).toBe(true);
      expect(DrugDealer.isPrototypeOf(drugDealer)).toBe(true);
      expect(Base.isPrototypeOf(drugDealer)).toBe(true);
      
      expect(drugDealer.obs).toMatch('Vendedor de metaanfetamina azul!');
      expect(drugDealer.name).toMatch('Heisenberg');
    });
  });
})();